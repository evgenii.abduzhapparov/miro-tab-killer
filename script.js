const extensionId = '<replace me>';

window.addEventListener('load', () => {
    chrome.runtime.sendMessage(extensionId, 'miro_page_loaded');
});
