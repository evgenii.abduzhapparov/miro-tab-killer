# Miro Tab Killer

If you use the Miro desktop application, you have probably noticed that every time you click a Miro board link,
it's opened in your browser and then in the app, leaving the resource-heavy tab open.

This extension closes the Miro board tab in your Chrome browser automatically.

## Installation

1. Clone the repository.
1. Open the Extensions page (Window -> Extensions) in your Chrome browser.
1. Enable the Developer mode in the right top corner.
1. Click the button "Load unpacked" and specify the path to the repository.
1. The extension will appear in the list. Copy its ID (it's always new), replace the value in `script.js` and save the file.
1. Reload the extension using the round arrow.

Now every time you open a Miro link this extension will close the tab.

If you decide to uninstall the Miro desktop app, just disable and remove this extension.
